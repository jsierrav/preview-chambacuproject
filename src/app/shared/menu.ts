export const DRINKS = [
    {
        name: "Cuba libre",
        price: "16.000"
    },
    {
        name: "Monto cubano",
        price: "16.000"
    },
    {
        name: "Margarita",
        price: "20.000"
    },
    {
        name: "Daikiri",
        price: "16.000"
    }
    , {
        name: "Dry martini",
        price: "20.000"
    },
    {
        name: "Piña colada",
        price: "20.000"
    },
    {
        name: "Tequila sunrise",
        price: "20.000"
    },
    {
        name: "Caipiriña",
        price: "20.000"
    }

]

export const OTHERDRINKS=[
    {
        name: "Cervezas nacionales",
        price: "8.000"
    },
    {
        name: "Cervezas nacionales micheladas",
        price: "11.000"
    },
    {
        name: "Cervezas importadas",
        price: "15.000"
    },
    {
        name: "Cerezas importadas micheladas",
        price: "17.000"
    }
    , {
        name: "Gaseosas, Agua botella, Tonic, Soda",
        price: "5.000"
    },
    {
        name: "Gatorade",
        price: "7.000"
    },
    {
        name: "Redbull",
        price: "15.000"
    },
    {
        name: "Smirnoff ice",
        price: "12.000"
    },
    {
        name: "Sumo de limon",
        price: "3.000"
    },
    
    
]


export const WINES=[
    {
        name: "De la casa",
        price: "55.000",
        priceDrink:'12.000'
    },
    {
        name: "Santa rita",
        price: "80.000",
        priceDrink:'16.000'
    },

    {
        name: "Casillero del diablo",
        price: "20.000",
        priceDrink:'19.000'
    }
]


export const RONES=[
    {
        name: "Havana club 7 años",
        price: "180.000",
        middle: '12.000',
        priceDrink:'20.000'
    },
    {
        name: "Havana club especial",
        price: "160.000",
        middle: '12.000',
        priceDrink:'28.000'
    },
    {
        name: "Havana club 3 años",
        price: "150.000",
        middle: '12.000',
        priceDrink:'16.000'
    },

    {
        name: "Zacapa 15 años",
        price: "200.000",
        middle: '12.000',
        priceDrink:'22.000'
    },
    {
        name: "Zacapa 23 años",
        price: "250.000",
        middle: '12.000',
        priceDrink:'25.000'
    } 
    , {
        name: "Medellin 12 años",
        price: "200.000",
        middle: '12.000',
        priceDrink:'16.000'
    },
    {
        name: "Medellin 8 años",
        price: "140.000",
        middle: '80.000',
        priceDrink:'15.000'
    },
    {
        name: "Rones nacionales",
        price: "100.000",
        middle: '80.000',
        priceDrink:'8.000'
    },
    
    
]



export const WHISKIES=[
    {
        name: "Buchanan's 18 años ",
        price: "320.000",
        middle: '',
        priceDrink:'25.000'
    },
    {
        name: "Buchanan's master",
        price: "260.000",
        middle: '',
        priceDrink:'23.000'
    },
    {
        name: "Buchanan's 12 años",
        price: "220.000",
        middle: '120.000',
        priceDrink:'20.000'
    },

    {
        name: "Old parr tribute",
        price: "260.000",
        middle: '',
        priceDrink:'23.000'
    },
    {
        name: "Old parr 12 años",
        price: "220.000",
        middle: '120.000',
        priceDrink:'20.000'
    } 
    , {
        name: "Chivas regal 18 años",
        price: "320.000",
        middle: '',
        priceDrink:'25.000'
    },
    {
        name: "Chivas regal 12 años",
        price: "220.000",
        middle: '120.000',
        priceDrink:'20.000'
    },
    {
        name: "Something special",
        price: "220.000",
        middle: '90.000',
        priceDrink:'16.000'
    },
    {
        name: "Double black",
        price: "260.000",
        middle: '',
        priceDrink:'23.000'
    },
    
    {
        name: "Sello negro",
        price: "220.000",
        middle: '120.000',
        priceDrink:'20.000'
    },
     
    {
        name: "Sello negro",
        price: "160.000",
        middle: '9.000',
        priceDrink:'26.000'
    },
    
]





export const OTHERLIQUORS=[
    {
        name: "Absolut",
        price: "190.000",
        middle: '110.000',
        priceDrink:'17.00'
    },
    {
        name: "Buchanan's master",
        price: "17.000",
        middle: '90.000',
        priceDrink:'15.000'
    },
    {
        name: "Smirnoff 12 años",
        price: "250.000",
        middle: '',
        priceDrink:'15.000'
    },

    {
        name: "Gin londos's",
        price: "260.000",
        middle: '',
        priceDrink:'23.000'
    },
    {
        name: "Olmeca deposado",
        price: "200.000",
        middle: '120.000',
        priceDrink:'16.000'
    } 
    , {
        name: "Don julio",
        price: "260.000",
        middle: '',
        priceDrink:'25.000'
    },
    {
        name: "Jose Cuervo",
        price: "180.000",
        middle: '100.000',
        priceDrink:'15.000'
    },
    {
        name: "Baileys",
        price: "150.000",
        middle: '',
        priceDrink:'18.000'
    },
    {
        name: "Double black",
        price: "260.000",
        middle: '',
        priceDrink:'23.000'
    },
    
    {
        name: "Sello negro",
        price: "220.000",
        middle: '120.000',
        priceDrink:'20.000'
    },
     
    {
        name: "Sello negro",
        price: "160.000",
        middle: '9.000',
        priceDrink:'26.000'
    },
    
]

export const ENTRADAS = [
    {
        name: "Ceviche de camarones",
        description:'A base de limón, cebolla roja y pimentón (125 grs)',
        price: "23.000"
    },
    {
        name: "Ceviche tradicional",
        description:'Camarones, pescado y maiz tierno',
        price: "28.000"
    },
    {
        name: "Coctel de camarones",
        description:'En una deliciosa salsa rosada (125 grs)',
        price: "28.000"
    },
    {
        name: "Patacon con sabor",
        description:'',
        price: "13.000"
    }
    ,
    {
        name: "Chicharroncitos cartageneros crocantes",
        description:'',
        price: "18.000"
    }
    ,
    {
        name: "Camarones tempura",
        description:'',
        price: "28.000"
    }

]
