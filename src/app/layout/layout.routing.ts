
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const appRoutes: Routes = [
    { path: '', component: LayoutComponent }
];

export const routing = RouterModule.forRoot(appRoutes);
