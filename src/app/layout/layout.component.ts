import { Component, OnInit } from '@angular/core';
import { DRINKS, OTHERDRINKS, WINES, RONES, WHISKIES,OTHERLIQUORS, ENTRADAS } from '../shared/menu';
import { AgmCoreModule } from '@agm/core';


@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
  title: string = 'My first AGM project';
  lat: number = 10.4246621;
  lng: number = -75.5522525;

  drinks: any[] = [];
  otherdrinks: any[] = [];
  wines: any[] = [];
  rones: any[] = [];
  whiskies: any[] = [];
  otherliquors: any[] = [];
  entradas:any[] = [];


  images;


  constructor() {
    this.images = [
      { url: 'assets/img1.jpg' },
      { url: 'assets/img2.jpg' },
      { url: 'assets/img3.jpg' },
      { url: 'assets/copas.jpg' },
      { url: 'assets/corona.jpg' },
      { url: 'assets/corona2.jpg' },
      { url: 'assets/sol.jpg' },
      { url: 'assets/icecream.jpg' },
      { url: 'assets/icecream2.jpg' },
      { url: 'assets/icecream3.jpg' },
      { url: 'assets/carne.jpg' },
      { url: 'assets/bebida2.jpg' },
      { url: 'assets/bebidas.jpg' },
      { url: 'assets/pizza.jpg' },
      { url: 'assets/coctel.jpg' },
      { url: 'assets/ensalada.jpg' }




    ];
  }

  ngOnInit() {
    this.drinks = DRINKS;
    this.otherdrinks = OTHERDRINKS;
    this.wines = WINES;
    this.rones = RONES;
    this.whiskies = WHISKIES;
    this.otherliquors = OTHERLIQUORS;
    this.entradas =ENTRADAS;

  }

}
