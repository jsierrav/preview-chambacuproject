import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ApplicationRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { LayoutComponent } from './layout/layout.component';
import {routing} from './layout/layout.routing';
import { AgmCoreModule } from '@agm/core';
import { GalleryComponent } from './gallery/gallery.component';


@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    GalleryComponent
  ],
  imports: [

    BrowserModule,
    CommonModule,
    FormsModule,
        routing,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyATtd3ptP5PyXorMWGFAm25ci02AP0N8WQ'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
