import { ChambacuProjectPage } from './app.po';

describe('chambacu-project App', () => {
  let page: ChambacuProjectPage;

  beforeEach(() => {
    page = new ChambacuProjectPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
